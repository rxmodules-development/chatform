import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component { 

    constructor(props){
        super(props);

        let pageConfig = JSON.parse(document.getElementById('config').innerHTML);

        let questionCount = pageConfig.questions.length;
        let attemptList = [];
        for (let i = 0; i < questionCount; i++) {
        	attemptList.push([]);
        }


        this.state = {
        	title: "Chat Window App",
        	intro: pageConfig.intro,
        	outro: pageConfig.outro,
        	questions: pageConfig.questions,
        	attempts: attemptList,
        	progress: 0,
        	responses: []
        }

        this._sendReply = this._sendReply.bind(this);
        this._handleKeyPress = this._handleKeyPress.bind(this);
    }

    render() {
        return (
            <div>
                {this._header()}
                {this._chatWindow()}
            </div>
        );
    }
    
    _header() {
        return (
            <header>
                <h1>{this.state.title}</h1>
            </header>
        );
    }
    
    _chatWindow() {
      	return (
            <div className="chat-window" id="chat-window">
            	{this._chatLog()}
                {this._inputBox()}
            </div>
        );
    }

    _chatLog() {
    	let outroMessage;
    	if (this.state.progress == this.state.questions.length) {
    		outroMessage = <ChatMessage 
                	key={this.state.questions.length + 1} 
                	text={this.state.outro}
                	user="bot"
                	type="single"
                />
    	}

    	return (
			<div className="chat-log" id="chat-log">
				<ChatMessage 
                	key="0" 
                	text={this.state.intro}
                	user="bot"
                	type="single"
                />
            	{this.state.questions.map((question, index) => {
                    if (index > this.state.progress) { return false };

                    return (
                        <ChatMessage 
                        	validation={this.state.questions[index].validation}
                        	attempts={this.state.attempts[index]}
                        	response={this.state.responses[index]}
                        	key={index + 1} 
                        	text={this.state.questions[index].value}
                        	user="bot"
                        	type="question"
                        />
                    );
                })}
                {outroMessage}
            </div>
    	)
    }

    _inputBox() {
    	return (
    		<div className="reply-input">
    			<input type="text" id="reply-input" className="reply-input__input" placeholder="Type Message..." onKeyPress={this._handleKeyPress}/>
    			<button className="reply-input__button" type="button" onClick={this._sendReply}>Send</button>
    		</div>
    	)
    }

    _clearInputAndFocus() {
    	document.getElementById('reply-input').value = "";
    	document.getElementById('reply-input').focus();
    }

    _updateScroll() { 
    	let scrollBox = document.getElementById('chat-log');
    		scrollBox.scrollTop = scrollBox.scrollHeight;
    }

    _handleKeyPress(e) {
	    if (e.key === 'Enter') {
	      	this._sendReply();
	    }
	 }

    _sendReply() {
    	let inputValue = document.getElementById('reply-input').value;
    	let emailTest = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    	let newResponses = this.state.responses;
    	let newAttempts = this.state.attempts;
    	let newProgress = this.state.progress + 1;

    	if (inputValue) {
    		if (this.state.questions[this.state.progress].type == "email") {
    			if (emailTest.test(inputValue)) {
    				newResponses.push(inputValue);
    				this.setState({responses: newResponses, progress: newProgress});
    				this._clearInputAndFocus();
    			} else {
    				newAttempts[this.state.progress].push(inputValue);
    				this.setState({attempts: newAttempts});
    			}
    		} else if (this.state.questions[this.state.progress].type == "number") {
    			if (!isNaN(inputValue)) {
    				newResponses.push(inputValue);
    				this.setState({responses: newResponses, progress: newProgress});
    				this._clearInputAndFocus();
    			} else {
    				newAttempts[this.state.progress].push(inputValue);
    				this.setState({attempts: newAttempts});
    			}
    		} else {
    			newResponses.push(inputValue);
    			this.setState({responses: newResponses, progress: newProgress});
    			this._clearInputAndFocus();
    		}
    	}

    	this._updateScroll();
    }   
}

class ChatMessage extends React.Component {
	constructor(props){
        super(props);
    }

    render() {
    	if (this.props.type == "single") {
    		return (
	            <div className={`chat-log__message chat-log__message-${this.props.user}`}>
	            	{this.props.text}
	            </div>
	        );
    	}

    	let response;
    	if (this.props.response) {
    		response = <div className="chat-log__message chat-log__message-user">{this.props.response}</div>
    	}

    	let attempts = [];
    	if (this.props.attempts) {
    		for (let i = 0; i < this.props.attempts.length; i++) {
    			attempts.push(<div key={i}><div className="chat-log__message chat-log__message-user">{this.props.attempts[i]}</div><div className="chat-log__message chat-log__message-bot">{this.props.validation.error}</div></div>);
    		}
    	}

    	return (
    		<div>
	    		<div className={`chat-log__message chat-log__message-${this.props.user}`}>
	            	{this.props.text}
	            </div>
	            {attempts}
	            {response}
	        </div>
    	)
    }
}

ReactDOM.render(<App/>,  document.getElementById("app"));