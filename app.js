import React from 'react';
import ReactDOM from 'react-dom';

let App = React.createClass({
    render: function () {
        return (
            <div>
                <Header title="Chat Bot Form"/>
            </div>
        );
    }
});

ReactDOM.render(<App/>,  document.getElementById("app"));